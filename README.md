# Userstyles

## Elk Square Avatars

![A screenshot of a part of the Elk page that has profile avatars, but they are square](previews/elk-square-avatars.png)

Changes the avatars on Elk to be rounded squares.

[Install](https://codeberg.org/basil/userstyles/raw/branch/main/styles/elk-square-avatars.user.css)